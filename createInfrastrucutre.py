from datetime import datetime
import json
import string
import argparse
import requests
import time


TIME_IN_SECONDS_BETWEEN_READS=5
TIME_IN_SECONDS_BETWEEN_INVOCATIONS=1
PE_HOST = "localhost"

dateFormat = "%Y%m%d_%H%M%S"

def doGET(url, key=None):
	r = requests.get(url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put(url, headers=getHeaders(key), data = json.dumps(data), verify=False)
	return r.status_code,r.text

def doPOST(url, data, key=None):
	r = requests.post(url, headers=getHeaders(key), data = json.dumps(data), verify=False)
	return r.status_code,r.text

def doDELETE(url, key=None):
	r = requests.delete(url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"
	if key != None:
		headers["key"] = key
	return headers

def doLogin(username, password):
	data=dict(user_name=username, user_password=password)
	r_s, r_t=doPOST('http://pkm-api_pkm_1:8080/user/login',data)
	response=json.loads(r_t)
	return response['key']

def createUser(userName, password, key):
	data=dict(name=userName, password=password)
	r_s, r_t=doPOST('http://pkm-api_pkm_1:8080/user', data, key)
	return json.loads(r_t)
	

def createProject(projectName, userName, key):
	data=dict(name=projectName, members=[dict(roles=['Developer'], owner=True, name=userName),dict(roles=['Developer',"Maintainer", "Reviewer"], owner=False, name="admin")])
	r_s, r_t=doPOST('http://pkm-api_pkm_1:8080/project', data, key)
	return json.loads(r_t)
	
def deleteUser(userName, key):
	return doDELETE('http://pkm-api_pkm_1:8080/user/'+userName, key)

def deleteProject(projectName, key):
	return doDELETE('http://pkm-api_pkm_1:8080/project/'+projectName, key)
	
def generateInfr(infrToCreate, key):
	for entry in infrToCreate:
		userName = entry['user']
		password = entry['passw']
		projectName = entry['project']
		print("DELETE PREEXISTING PROJECT: ", projectName, deleteProject(projectName, key))
		print("DELETE PREEXISTING USER: ", userName, deleteUser(userName, key))

		print("CREATE USER: ", userName, createUser(userName, password, key))
		print("CREATE PROJECT: ", projectName, createProject(projectName, userName, key))

def generateUsers(numOfUsers, key):
	infrToCreate=[]
	for i in range(numOfUsers):
		infrToCreate.append(dict(user="prueba"+str(i+1), passw="prueba"+str(i+1)+"Pass", project="prueba"+str(i+1)+"Project"))

	generateInfr(infrToCreate, key)

key = doLogin("admin", "cN6jK7eU5bM3")


infrToCreate=[]
infrToCreate.append(dict(user="malbert", passw="malbertPass", project="malbertProject"))
infrToCreate.append(dict(user="mgil", passw="mgilPass", project="mgilProject"))
generateInfr(infrToCreate, key)
#generateUsers(1)




