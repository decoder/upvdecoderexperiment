#!flask/bin/python
from flask import Flask, jsonify, abort, Response
import os, sys
from flask import request
import random, string, json
from requests.utils import quote
from os import listdir
from os.path import isfile, join
import requests
import json
import urllib3
import random, string

from datetime import datetime

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PKM_API_HOST="pkm"

app = Flask(__name__)
app.debug = True
app.reloader = True


def doPOST(url, data, key=None):
	r = requests.post('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doPUT(url, data, key=None):
	r = requests.put('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), data = data, verify=False)
	return r.status_code,r.text

def doGET(url, key=None):
	r = requests.get('http://' + PKM_API_HOST + ':8080' + url, headers=getHeaders(key), verify=False)
	return r.status_code,r.text

def getHeaders(key=None):
	headers = {}
	headers["Content-Type"] = "application/json; charset=utf-8"
	headers["Accept"] = "application/json"
	headers["Access-Control-Allow-Origin"]= "*"
	headers["Access-Control-Allow-Headers"]= "*"
	headers["Access-Control-Allow-Methods"]= "*"

	if key != None:
		headers["key"] = key
	return headers

def createResponse(data, status=200):
	r=Response(response=json.dumps(data, indent=4), status=status)
	r.headers["Content-Type"] = "application/json; charset=utf-8"
	r.headers["Access-Control-Allow-Origin"]= "*"
	r.headers["Access-Control-Allow-Headers"]= "*"
	r.headers["Access-Control-Allow-Methods"]= "*"
	return r

def doLogin(username, password):
	if username==None  and password==None:
		pload = {"user_name":"admin", "user_password":"cN6jK7eU5bM3"}
	else:
		pload = {"user_name":username, "user_password":password}
	url="/user/login"
	resStatus, resLogin=doPOST(url,str(pload).replace("'",'"'))
	key=(json.loads(resLogin))['key']
	return key

